/**
 * 1. Создаем экземпляр в глобальной видимости
 * window.pageMenu = new PageMenu();
 *
 * 2. Рисуем его в DOM и передаем модель и обработчик с контекстом
 * pageMenu.draw(SC._model, SC.moveToLevel.bind(SC));
 *
 * 3. При перерисовке только новую модель
 * pageMenu.redraw(SC._model);
 *
 * 4. Метод для управления
 * pageMenu.setMenuPosition(level) - переход по уровням
 * pageMenu.setMenuPosition(level, slide) - переход по слайдам текущего уровня
 */

(function (w, d, $, undefined) {
  'use strict';

  function PageMenu() {
    this.curPlace = {
      level: 0,
      slide: 0
    };
    this.levelMoveFn = null;
  }

  PageMenu.prototype = {
    draw: function (m, levelMoveFn) {
      this.generateMenu(m);
      this.setMenuPosition(0, 0);
      this.addMenuClickListener(levelMoveFn);
      this.levelMoveFn = levelMoveFn;
    },
    redraw: function (m) {
      this.generateMenu(m);
      this.setMenuPosition(0, 0);
      this.addMenuClickListener(this.levelMoveFn);
    },
    /**
     * Генератор структуры меню на основе model
     *
     */
    generateMenu: function (m) {
      if ($('.page-menu-container')) {
        $('.page-menu-container').fadeOut(500, function () {
          this.remove();
        });
      }

      var menuContent = '';

      var levels = m.length;

      for (var i = 0; i < levels; i++) {
        var childHTML = '';
        var menuContentItem = '';

        var slides = ((i === 0) ? 0 : m[i]);

        for (var j = 0; j < slides; j++) {
          childHTML += '<div class="page-item" id="page-item' + i + j + '"><div class="page-circle"></div></div>';
        }

        menuContentItem += '<div class="page-menu" id="page-menu' + i + '">';
        menuContentItem += '<div class="page-menu-main"></div>';
        menuContentItem += '<div class="page-menu-longline" id="page-menu-longline' + i + '">';
        menuContentItem += childHTML;
        menuContentItem += '</div></div>';

        menuContent += menuContentItem;
      }

      var menuHTML = $('<div/>', {
        id: 'page-menu-container',
        class: 'page-menu-container',
        html: menuContent
      });

      $('body').prepend(menuHTML);

      //Динамический расчет высоты контейнера меню
      var pageMenuDiv = $('.page-menu');
      var menuItemFullHeight = pageMenuDiv.outerHeight(true);
      var menuItemHeight = pageMenuDiv.height();
      var menuHeight = (menuItemFullHeight + menuItemHeight) / 2 * levels + (menuItemFullHeight - menuItemHeight) / 2;
      $('.page-menu-container').height(menuHeight);

      $('.page-menu-container').fadeIn(500);
    },
    addMenuClickListener: function (levelMoveFn) {
      /**
       * Переход на уровни кликами по меню
       *
       */
      $('.page-menu-main').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var goToScreenNumber = $('.page-menu-main').index(this);
        levelMoveFn({level: goToScreenNumber});
      }).mousedown(function (e) {
        e.preventDefault();
        e.stopPropagation();
      }).mouseup(function (e) {
        e.preventDefault();
        e.stopPropagation();
      });

      /**
       * Клики по краям экрана
       *
       */

      $('.sidebar.nav-top').click(function (e) {
        var goToScreenNumber = pageMenu.curPlace.level - 1;
        levelMoveFn({level: goToScreenNumber});
      });

      $('.sidebar.nav-bottom').click(function (e) {
        var goToScreenNumber = pageMenu.curPlace.level + 1;
        levelMoveFn({level: goToScreenNumber});
      });

      $('.sidebar.nav-left').click(function (e) {
        var goToSlideNumber = pageMenu.curPlace.slide - 1;
        levelMoveFn({slide: goToSlideNumber});
      });

      $('.sidebar.nav-right').click(function (e) {
        var goToSlideNumber = pageMenu.curPlace.slide + 1;
        levelMoveFn({slide: goToSlideNumber});
      });
    },
    setMenuPosition: function (level, slide) {
      //Меняем круг на квадрат у текущего уровня
      if ($('.page-menu-main').hasClass('current-level')) {
        $('.page-menu-main').removeClass('current-level');
      }
      $('#page-menu' + level).children('.page-menu-main').addClass('current-level');

      if (this.curPlace.level !== level) {
        this.showLevelSlidesInMenu(level);
      } else if (this.curPlace.slide !== slide) {
        if (this.curPlace.slide < slide) {
          this.moveMenuSlide('left');
        }
        if (this.curPlace.slide > slide) {
          this.moveMenuSlide('right');
        }
      }

      this.curPlace.level = level;
      this.curPlace.slide = slide;

      if (this.curPlace.level === 0 && this.curPlace.slide === 0) {
        setTimeout(function () {
          $('.page-menu-main').css('background', '#E84D30');
          $('.page-menu-main').css('border', '#ffffff 3px solid');
          $('.page-circle').css('background', '#ffffff');

          $('#logoSvg').children('span').each(function () {
            $(this).removeClass('notZeroLevel').addClass('zeroLevel');
          });
        }, 500);

      } else {
        setTimeout(function () {
          $('.page-menu-main').css('background', '#ffffff');
          $('.page-menu-main').css('border', '#424242 3px solid');
          $('.page-circle').css('background', '#424242');

          $('#logoSvg').children('span').each(function () {
            $(this).removeClass('zeroLevel').addClass('notZeroLevel');
          });
        }, 500);
      }
    },
    /**
     * Последовательная анимация появления пунктов меню ("слайдов") в текущем уровне
     *
     */
    showLevelSlidesInMenu: function (level) {
      $('.page-menu-longline').css({
        marginLeft: "0"
      });
      $('.page-menu-longline').hide();

      var curLevel = level;
      var slides = $('#page-menu-longline' + curLevel).children();
      var slidesCount = slides.length;
      var slidesMargins = [];

      for (var i = 0; i < slidesCount; i++) {
        slidesMargins[i] = $(slides[i - 1]).width() + slidesMargins[i - 1] || 0;
      }

      for (var j = 0; j < slidesCount; j++) {
        $(slides[j]).css('margin-left', -slidesMargins[j]);
      }

      $(function () {
        var animTime = 300;
        $($('#page-menu-longline' + curLevel)).fadeIn(animTime + 400);

        for (var k = 0; k < slidesCount; k++) {
          $(slides[k]).animate({
            marginLeft: '0px',
            opacity: 1
          }, {
            duration: animTime,
            queue: false
          });
        }
      });
    },
    moveMenuSlide: function (direction) {
      if (direction === 'left') {
        $('#page-menu-longline' + this.curPlace.level).animate({
          marginLeft: '-=20',
          opacity: 1
        }, {
          duration: 200
        });
      }

      if (direction === 'right') {
        $('#page-menu-longline' + this.curPlace.level).animate({
          marginLeft: '+=20',
          opacity: 1
        }, {
          duration: 200
        });
      }
    }
  };

  w.PageMenu = PageMenu;

})(window, document, jQuery);