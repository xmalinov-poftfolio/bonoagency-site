function init_map() {
  var myOptions = {
    zoom: 17,
    center: new google.maps.LatLng(57.600559, 39.876105999999936),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    panControl: false,
    zoomControl: false,
    scaleControl: false,
    disableDefaultUI: true
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
  marker = new google.maps.Marker({
    map: map,
    position: new google.maps.LatLng(57.600559, 39.876105999999936)
  });
  infowindow = new google.maps.InfoWindow({
    content: "<b>Bonoagency</b><br/>Moskovsky, 93<br/>150030 Yaroslavl"
  });
  google.maps.event.addListener(marker, 'click', function () {
    infowindow.open(map, marker);
  });
  infowindow.open(map, marker);
}
google.maps.event.addDomListener(window, 'load', init_map);