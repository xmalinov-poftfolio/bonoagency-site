String.prototype.format = function () {
  var arg = arguments;
  return this.replace(/\{(\d+)\}/g, function (a, b) {
    return arg[b]
  });
};

(function (w, d, $) {
  var _framing = function (opts) {
    this.el = $(this.getSelector('framingClass'));
    var framePattern = '<div class="'
      + this.getSelector('itemClass').substr(1)
      + ' {0}'
      + '">{1}</div>';
    var frames = ['', ''];
    for (var i = 0; i < opts.amount; ++i) {
      var borderColor = opts.colors[i % opts.colors.length];
      frames[0] += framePattern.format(borderColor, '');
      frames[1] += framePattern.format(borderColor, '');
    }
    for (var j = 0; j < this.el.length; ++j)  //сделано на случай, если будет разное содержимое у рамок
      $(this.el[j]).append(frames[j]);

    var firstEl = this.el.find(this.getSelector('itemClass') + ':first');
    var firstElBorder = [
      parseInt(firstEl.css('border-top-width')),
      parseInt(firstEl.css('border-right-width')),
      parseInt(firstEl.css('border-bottom-width')),
      parseInt(firstEl.css('border-left-width'))
    ];
    this.setCss({
      'top': firstEl.offset().top,
      'margin-left': firstEl.offset().left,
      'border-width': this.el.hasClass('top') ? firstElBorder[0] + firstElBorder[2] : firstElBorder[1] + firstElBorder[3],
      'default-margin-left': parseInt(this.el.find(this.getSelector('lineClass')).css('margin-left'))
    });
  };

  _framing.prototype = {
    _selectors: {
      'framingClass': '.framing',
      'itemClass': '.frame-item',
      'hLineClass': '.horisontal',
      'vLineClass': '.vertical',
      'lineClass': '.line'
    },
    _css: {
      'top': 0,
      'margin-left': 0,
      'border-width': 0,
      'default-margin-left': 0
    },
    _progress: {
      'total': 0,
      'animation': 0
    },
    _maxDistance: 50,

    _moveFuncInOut: function (val) {
      return this._normalizeMoveFunc(parseFloat(Math.sin(val * Math.PI).toFixed(15)));
    },

    moveByValue: function (val, isVertical) {
      if (isVertical) {
        this.el.css('top', val);
        var zVal = -parseInt(this._moveFuncInOut(this.getProgress()) * this._maxDistance);
        var cssObj = {
          '-webkit-transform': 'translateZ( ' + zVal + 'px)', // Safari
          'transform': 'translateZ( ' + zVal + 'px)'
        };
        this.el.find(this.getSelector('itemClass')).css(cssObj);
      }
    },

    resize: function (topObj) {
      var screenSize = {
        height: document.body.clientHeight,
        width: document.body.clientWidth
      };
      var cssObj = {
        'width': screenSize.width - this.getCssLeft() * 2 - this.getCssBorderWidth(),
        'height': screenSize.height - this.getCssTop() * 2 - this.getCssBorderWidth()
      };
      if (topObj)
        this.el.css(topObj);
      var frames = this.el.find(this.getSelector('itemClass'));
      for (var i = 0; i < frames.length; ++i) {
        var frame = $(frames[i]);
        if (i == 0 || i == frames.length / 2)
          frame.css($.extend($.extend({}, cssObj), {
            'height': screenSize.height - this.getCssTop() - this.getCssBorderWidth()
          }));
        else
          frame.css(cssObj);
      }
    },

    setProgress: function (progress, isAnimation) {
      var tmp = progress && progress >= 0 ? progress : 0;
      if (isAnimation) {
        this._progress['animation'] = tmp;
      }
      else {
        this._progress['total'] = tmp;
        this._progress['animation'] = 0;
        this._progress.isReverted = undefined;
      }
    },
    revertProgress: function () {
      if (!this._progress.isReverted)
        this._progress['total'] = 1 - this._progress['total'];
      this._progress.isReverted = true;
    },
    getProgress: function () {
      var progress = this._progress['total'];
      var animationProgress = this._progress['animation'];
      if (animationProgress)
        progress = progress + (1 - progress) * this._progress['animation'];
      if (animationProgress == 1) {
        this.setProgress();
      }
      return progress;
    },

    setCss: function (cssObj) {
      $.extend(this._css, cssObj);
    },

    getCssTop: function () {
      return this._css['top'];
    },
    getCssLeft: function () {
      return this._css['margin-left'];
    },
    getCssBorderWidth: function () {
      return this._css['border-width'];
    },

    getSelector: function (name) {
      return this._selectors[name] || null;
    }
  };

  w.Framing = _framing;
})(window, document, jQuery);
