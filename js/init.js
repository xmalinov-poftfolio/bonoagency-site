$(document).ready(function () {
  window.SC = new Scroll();

  var page = SC.el;
  var levels = page.find(SC.getSelector('levelClass'));

  window.pageMenu = new PageMenu();
  pageMenu.draw(SC.getCurModel(), SC.moveToScreen.bind(SC));

  // для установки различных цветов фона
  $.fn.setDifferentBackground = function () {
    var slides = levels.find(SC.getSelector('slideClass'));
    var getColor = function () {
      return parseInt(Math.random() * 255).toString();
    };
    var getFullColor = function () {
      var color = [];
      for (var i = 0; i < 3; ++i)
        color.push(getColor());
      return color;
    };
    var invertFullColor = function (color) {
      var invertedColor = [];
      for (var i = 0; i < color.length; ++i) {
        invertedColor.push(256 - color[i]);
      }
      return invertedColor;
    };
    for (var i = 0; i < slides.length; ++i) {
      var bgColor = getFullColor();
      var color = invertFullColor(bgColor);
      $(slides[i]).css({
        'background-color': ('rgb(' + bgColor.join(',') + ')' )
        //, 'color': ('rgb(' + color.join(',') + ')' )
      });
    }
    return this;
  };

  /*for ( var i = 0; i < levels.length; ++i ) {
   $( levels[ i ] ).setDifferentBackground();
   }*/
});