(function (w, d, $) {
  var _animate = function (opts) {
    this.setOptions(opts);
  };

  _animate.prototype = {
    _timer: null,
    delay: 0,
    duration: 0,
    obj: null,
    methodName: null,
    complete: function () {
    },

    setOptions: function (opts) {
      $.extend(this, opts);
    },

    start: function (curProgress) {
      var startTime = new Date;

      this._timer = setInterval(function () {
        var duration = this.duration * (curProgress || 1);
        var progress = (new Date - startTime) / duration;
        if (progress > 1)
          progress = 1;

        if (progress == 1)
          this.finish();
        else
          this.obj[this.methodName](progress);
      }.bind(this), this.delay);
    },

    finish: function () {
      this.obj[this.methodName](1);
      clearInterval(this._timer);
      this._timer = undefined;
      this.complete && this.complete();
    },

    stop: function () {
      if (this._timer)
        this.finish();
    }
  };

  w.Animation = _animate;
})(window, document, jQuery);
