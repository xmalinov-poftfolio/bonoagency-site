(function (w) {
  'use strict';

  var lineConst = 28;
  var lineHeightMod = 1.2;
  var contentNoMod = 1;
  var contentHeaderMod = 0.85;
  var contentTextMod = 0.6;
  var contentIconMod = 0.5;

  function setContentFontSize(selector, lMod) {
    var winHeight = $(w).height();

    $(selector).css('font-size', Math.round((winHeight / lineConst) * lMod) + 'px');
    $(selector).css('line-height', Math.round((winHeight / lineConst * lineHeightMod) * lMod) + 'px');
  }

  function setIconSize(selector, lMod) {
    var winHeight = $(w).height();

    $(selector).css('width', Math.round((winHeight / lineConst) * lMod) + 'px');
  }

  function setContentIconSize(selector, lMod) {
    var winHeight = $(w).height();
    $(selector).css('height', Math.round((winHeight / lineConst) * lMod) + 'px');
  }

  function setContentTop() {
    var winHeight = $(w).height();
    var topMargin = 0.354476 * winHeight - 9.957;

    $('.slide h1').css('margin-top', Math.round(topMargin) + 'px');
  }

  setContentFontSize('.slide h1', contentNoMod);
  setContentFontSize('h1.first-page', 1.3);
  setContentFontSize('.slide .text-h', contentHeaderMod * 0.9);
  setContentFontSize('.one-column.address', contentHeaderMod * 1.2);
  setContentFontSize('.slide .text-column', contentTextMod);
  setContentFontSize('.slide .column-logo-h-small', contentIconMod);
  setIconSize('.slide .column-logo-small', 1);
  //setContentIconSize('.slide .column-logo-small object', 1.2);
  setContentTop();

  $(w).on('resize', function () {
    setContentFontSize('.slide h1', contentNoMod);
    setContentFontSize('h1.first-page', 1.3);
    setContentFontSize('.slide .text-h', contentHeaderMod * 0.9);
    setContentFontSize('.one-column.address', contentHeaderMod * 1.2);
    setContentFontSize('.slide .text-column', contentTextMod);
    setContentFontSize('.slide .column-logo-h-small', contentIconMod);
    setIconSize('.slide .column-logo-small', 1);
    //setContentIconSize('.slide .column-logo-small object', 1.2);
    setContentTop();
  });

})(window);