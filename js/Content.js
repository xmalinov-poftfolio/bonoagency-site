String.prototype.format = function () {
  var arg = arguments;
  return this.replace(/\{(\d+)\}/g, function (a, b) {
    return arg[b]
  });
};

(function (w, d, $) {
  var _content = function (opts) {
    this.el = $(this.getSelector('contentClass'));

    this._curScreen = opts.curScreen;
    this.setStructure(opts.structure);

    var $firstMap = this.el.find(this.getSelector('mapClass') + ':first');
    this.setCss({
      'max-margin-left': this.el.find(this.getSelector('headerClass') + ':first').offset().left
    });
    this.setMapCss({
      'min-height': $firstMap.height(),
      'min-width': $firstMap.width(),
      'min-margin-top': parseInt($firstMap.css('margin-top')),
      'min-margin-left': parseInt($firstMap.css('margin-left'))
    });
    for (var model in this._structure) {
      var $levels = this.el.find(this.getSelector('levelClass') + '.' + model);
      for (var i = 0; i < $levels.length; ++i) {
        var $level = $($levels[i]);
        var headers = $level.find(this.getSelector('headerClass'));

        for (var j = 0; j < headers.length; ++j) {
          var $header = $(headers[j]);
          var hWidth = $header.width();
          var leftSide = Math.abs(this.getCssDefaultMinLeft()) + hWidth;
          var width = leftSide + hWidth + this.getCssDefaultMaxLeft();
          // ошибка с недоезжанием связана с подгружаемым шрифтом
          // в момент инициализации шрифт один,
          // когда подгружается нужный и применяется, размер текста увеличивается
          // и при использовании мы опираемся на текущий размер текста с примененным шрифтом
          // как вариант запоминать у какого заголовка какой размер был и работать с этим размером
          this.setHeaderWidth(model, i, j, hWidth);
          if (!$header.siblings('.line').length)
            $header.after('<div class="line {0}" style="width:{1}px; margin-left: -{2}px"></div>'.format(opts.colors[i % opts.colors.length], width, leftSide));
        }
      }
    }
  };

  _content.prototype = {
    _selectors: {
      contentClass: '.content',
      levelClass: '.level',
      slideClass: '.slide',
      headerClass: 'h1',
      textClass: '.text',
      lineClass: '.line',
      mapClass: '.map',
      mapCanvasClass: '.map-canvas'
    },
    _css: {
      'max-margin-left': 0,
      'min-margin-left': -100
    },
    _mapCss: {
      'max-margin': 58,
      'min-margin-left': 0,
      'min-margin-top': 0,
      'max-width': 0,
      'max-height': 0,
      'min-width': 0,
      'min-height': 0
    },
    _curScreen: {
      'level': 0,
      'slide': 0
    },
    _headerWidth: {},
    _pairOfHeaders: [],
    _structure: [],
    _progress: {
      'total': 0,
      'animation': 0
    },
    _moveByArrows: false,
    _maxDistance: 50,

    _normalizeHeadersInLevel: function ($headers, model, level) {
      $headers = $headers || this.getLevel(level).find(this.getSelector('headerSelector'));
      for (var i = 0; i < $headers.length; ++i) {
        var $header = $($headers[i]);
        var $map = $header.siblings(this.getSelector('mapClass'));
        if ($header)
          switch (i) {
            case 0 :
              $header.css({
                'margin-left': this.getCssDefaultMaxLeft()
              });
              break;
            default :
              $header.css({
                'margin-left': -this.getHeaderWidth(model, level, i) + this.getCssDefaultMinLeft()
              });
              $map.css({
                'margin-left': parseInt($header.css('margin-left')) + 100,
                'height': (document.body.clientHeight - this.getMaxMapMargin() * 2) / 2
              });
              break;
          }
      }
    },

    _moveFuncInOut: function (val) {
      return this._normalizeMoveFunc(parseFloat(Math.sin(val * Math.PI).toFixed(15)));
    },

    moveByDelta: function (delta, isVertical) {
      if (delta == 0)
        return;
      var header = this.el.find(this.getSelector('headerClass'));
      var text = this.el.find(this.getSelector('textClass'));
      var line = this.el.find(this.getSelector('lineClass'));

      if (isVertical) {
        var zVal = -parseInt(this._moveFuncInOut(this.getProgress()) * this._maxDistance);
        var transformCssObj = {
          '-webkit-transform': 'translateZ( ' + zVal + 'px)', // Safari
          'transform': 'translateZ( ' + zVal + 'px)'
        };
        this.el.children().css(transformCssObj);

        //header.css( opacityObj );
        //line.css( opacityObj );
      }
      else {
        var opacityObj = {'opacity': 1 - this._moveFuncInOut(this.getProgress()) * 0.8};
        text.css(opacityObj);

        var moveByArrowsFactor = (this.getMoveByArrows() ? (delta / Math.abs(delta)) : 0);
        var curLevel = this.getCurScreenLevel();
        var curSlide = this.getCurScreenSlide() + moveByArrowsFactor;
        var deltaMapHeight = ((document.body.clientHeight - this.getMaxMapMargin() * 2) / 2);
        this.setPairOfHeaders([
          {
            level: curLevel,
            slide: curSlide
          }, {
            level: curLevel,
            slide: curSlide - (delta / Math.abs(delta))
          }
        ]);

        var maxLeft = this.getCssDefaultMaxLeft();
        var minLeft = this.getCssDefaultMinLeft();
        var progressByDelta = Math.abs(delta) / document.body.clientWidth;

        var curHeader = this.getHeaderFromPair(0);
        var curHeaderFactor = curHeader['margin-left'] < 0 ? 1 : -1;
        var curHeaderMinMarginLeft = this.getHeaderWidth(curHeader.model, curHeader.level, curHeader.slide) - minLeft;
        var h0Left = curHeader['margin-left']
          + Math.abs(maxLeft + curHeaderMinMarginLeft)
          * progressByDelta
          * curHeaderFactor;
        curHeader.el.css('margin-left', h0Left);

        curHeader['map'].el.css({
            'margin-left': curHeader['map']['margin-left']
            + Math.abs(this.getMaxMapMargin() + curHeaderMinMarginLeft - 100)
            * progressByDelta
            * curHeaderFactor,
            'height': curHeader['map']['height']
            + deltaMapHeight
            * progressByDelta
            * curHeaderFactor
          }
        );

        var nextHeader = this.getHeaderFromPair(1);
        if (nextHeader) {
          var nextHeaderFactor = nextHeader['margin-left'] < 0 ? 1 : -1;
          var nextHeaderMinMarginLeft = this.getHeaderWidth(nextHeader.model, nextHeader.level, nextHeader.slide) - minLeft;
          var h1Left = nextHeader['margin-left']
            + Math.abs(maxLeft + nextHeaderMinMarginLeft)
            * progressByDelta
            * nextHeaderFactor;
          nextHeader.el.css('margin-left', h1Left);
          nextHeader['map'].el.css({
              'margin-left': nextHeader['map']['margin-left']
              + Math.abs(this.getMaxMapMargin() + nextHeaderMinMarginLeft - 100)
              * progressByDelta
              * nextHeaderFactor,
              'height': nextHeader['map']['height']
              + deltaMapHeight
              * progressByDelta
              * nextHeaderFactor
            }
          );
        }
      }
    },

    resize: function (top) {
      this.el.find(this.getSelector('mapCanvasClass')).css({
        'height': document.body.clientHeight - this.getMaxMapMargin() * 2,
        'width': document.body.clientWidth - this.getMaxMapMargin() * 2
      });
      this.el.find(this.getSelector('mapClass')).css({
        'margin-left': this.getMaxMapMargin()
      });
      this.normaliseHeaders();
    },

    normaliseHeaders: function (level) {
      var model = this.getModel();
      if (typeof level === 'number' && level >= 0 && level < model.length) {
        this._normalizeHeadersInLevel(this.getLevel(level).find(this.getSelector('headerClass')), this.getCurScreenModel(), level);
      }
      else {
        for (var model in this._structure) {
          var $levels = this.el.find(this.getSelector('levelClass') + '.' + model);
          for (var i = 0; i < $levels.length; ++i) {
            var $level = $($levels[i]);
            this._normalizeHeadersInLevel($level.find(this.getSelector('headerClass')), model, $level.attr('index'));
          }
        }
      }
    },

    setProgress: function (progress, isAnimation) {
      var tmp = progress && progress >= 0 ? progress : 0;
      if (isAnimation) {
        this._progress['animation'] = tmp;
      }
      else {
        this._progress['total'] = tmp;
        this._progress['animation'] = 0;
        this._progress.isReverted = false;
      }
    },
    revertProgress: function () {
      if (!this.isProgressReverted())
        this._progress['total'] = 1 - this._progress['total'];
      this._progress.isReverted = true;
    },
    getProgress: function () {
      var progress = this._progress['total'];
      var animationProgress = this._progress['animation'];
      if (animationProgress)
        progress = progress + (1 - progress) * this._progress['animation'];
      if (animationProgress == 1) {
        this.setProgress(0);
      }
      return progress;
    },
    isProgressReverted: function () {
      return !!this._progress.isReverted;
    },

    setCss: function (cssObj) {
      $.extend(this._css, cssObj);
    },

    getCssDefaultMaxLeft: function () {
      return this._css['max-margin-left'];
    },
    getCssDefaultMinLeft: function () {
      return this._css['min-margin-left'];
    },

    setMapCss: function (cssObj) {
      $.extend(this._mapCss, cssObj);
    },
    getMaxMapMargin: function () {
      return this._mapCss['max-margin'];
    },
    getMinMapMarginLeft: function () {
      return this._mapCss['min-margin-left'];
    },
    getMinMapMarginTop: function () {
      return this._mapCss['min-margin-top'];
    },
    getMaxMapHeight: function () {
      return this._mapCss['max-height'];
    },
    getMaxMapWidth: function () {
      return this._mapCss['max-width'];
    },
    getMinMapHeight: function () {
      return this._mapCss['min-height'];
    },
    getMinMapWidth: function () {
      return this._mapCss['min-width'];
    },

    setStructure: function (structure) {
      this._structure = structure;
    },
    getModel: function () {
      return this._structure[this.getCurScreenModel()];
    },
    getCurScreenModel: function () {
      return this._curScreen.model;
    },
    getCurScreenLevel: function () {
      return this._curScreen.level
    },
    getCurScreenSlide: function () {
      return this._curScreen.slide;
    },

    setPairOfHeaders: function (headers) {
      if (this._pairOfHeaders.length)
        return;
      for (var i = 0; i < headers.length; ++i) {
        var $header = this.getHeader(headers[i].slide);
        var curMarginLeft = $header && parseInt($header.css('margin-left'));
        var leftMargin = $header && this.getCssDefaultMinLeft() - this.getHeaderWidth(this.getCurScreenModel(), headers[i].level, headers[i].slide);
        var mapMaxHeight = document.body.clientHeight - this.getMaxMapMargin() * 2;
        this._pairOfHeaders.push($header && {
            'el': $header,
            'margin-left': curMarginLeft < 0
              ? leftMargin
              : this.getCssDefaultMaxLeft(),
            'model': this.getCurScreenModel(),
            'level': headers[i].level,
            'slide': headers[i].slide,
            'map': {
              'el': $header.siblings(this.getSelector('mapClass')),
              'margin-left': curMarginLeft < 0
                ? (leftMargin + 100)
                : this.getMaxMapMargin(),
              'height': curMarginLeft < 0
                ? mapMaxHeight / 2
                : mapMaxHeight
            }
          });
      }
    },
    freePairOfHeaders: function () {
      this._pairOfHeaders.length = 0;
    },
    getHeaderFromPair: function (id) {
      return typeof id === 'number' ? this._pairOfHeaders[id] : this._pairOfHeaders;
    },

    getHeader: function (slide) {
      var $slide = this.getSlide(slide);
      return $slide.length ? $slide.find(this.getSelector('headerClass')) : null;
    },

    getSelector: function (name) {
      return this._selectors[name];
    },

    setHeaderWidth: function (model, level, slide, width) {
      this._headerWidth[model + '_' + level + '_' + slide] = width;
    },
    getHeaderWidth: function (model, level, slide) {
      return this._headerWidth[model + '_' + level + '_' + slide];
    },

    setMoveByArrows: function (canChange) {
      this._moveByArrows = canChange;
    },
    getMoveByArrows: function () {
      return this._moveByArrows
    }
  };

  w.Content = _content;
})(window, document, jQuery);
