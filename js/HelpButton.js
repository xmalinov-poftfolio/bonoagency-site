(function (w, d, $) {
  var _helpButton = function (opts) {
    this.el = $(this.getSelector('helpButton'));
    this.el.addClass(opts.color);
    this.setAnimation({
      delay: 5,
      duration: 150,
      obj: this,
      methodName: 'moveByProgress'
    });
    this.setCss({
      'min-right': -parseInt(this.el.width())
    });
    this._curScreen = opts._curScreen;
  };

  _helpButton.prototype = {
    _selectors: {
      helpButton: '.help-button'
    },
    _animation: null,
    _isVisible: true,
    _css: {
      'max-right': 0,
      'min-right': 0
    },
    _canToggle: true,
    _curScreen: {
      'level': 0
    },

    toggle: function (blockToggle) {
      if (( this.getCurScreenLevel() === 0 && !this._isVisible)
        || (this.getCurScreenLevel() !== 0 && this._isVisible)
        && this.canToggle()
      ) {
        var animation = this.getAnimation();
        animation.stop();
        animation.setOptions({
          complete: this.toggleVisibility.bind(this)
        });
        animation.start();
      }
      if (blockToggle)
        this.setToggle(true);
    },

    moveByProgress: function (progress) {
      var curRight = this._isVisible ? this.getMaxRight() : this.getMinRight();
      var factor = this._isVisible ? -1 : 1;
      this.el.css({
        'right': (
          curRight
          + Math.abs(this.getMaxRight() - this.getMinRight())
          * progress
          * factor
        )
      });
    },

    resize: function (top) {
    },

    getCurScreenLevel: function () {
      return this._curScreen.level;
    },

    setAnimation: function (opts) {
      this._animation = new Animation(opts);
    },
    getAnimation: function () {
      return this._animation;
    },

    setCss: function (obj) {
      $.extend(this._css, obj);
    },

    getMaxRight: function () {
      return this._css['max-right'];
    },
    getMinRight: function () {
      return this._css['min-right'];
    },

    toggleVisibility: function () {
      this._isVisible = !this._isVisible;
    },

    getSelector: function (name) {
      return this._selectors[name] || "";
    },

    setToggle: function (canToggle) {
      this._canToggle = canToggle;
    },
    canToggle: function () {
      return this._canToggle;
    }
  };

  w.HelpButton = _helpButton;
})(window, document, jQuery);