(function (w, d, $) {
  var _scroll = function () {
    this.el = $(this.getSelector('contentClass'));

    this.initStructure();
    this.setIds(this.el.find(this.getSelector('levelClass')));
    this.setStructure();

    $(document.body).on(this.events.join(' '), this.handleEvent.bind(null, this));

    this.setAnimation({
      delay: 10,
      duration: 1000,
      obj: this,
      methodName: 'moveByProgress'
    });

    $.extend(this.setContent({
      structure: this._structure,
      colors: this._borderColors,
      curScreen: this._curScreen
    }), {
      //TODO: как-то вынести это всё в базовый объект, от которого можно будет наследоваться
      getLevel: this.getLevel.bind(this),
      getSlide: this.getSlide.bind(this),
      _normalizeMoveFunc: this._normalizeMoveFunc.bind(this)
    });

    $.extend(this.setFraming({
      amount: this.getLevelsAmount(),
      colors: this._borderColors
    }), {
      _normalizeMoveFunc: this._normalizeMoveFunc.bind(this)
    });

    this.showCurModelLevels();

    this.setCursorStyle();

    this.addClicksOnHeaders();

    this.resize();

    $(w).resize(this.resize.bind(this));
  };

  _scroll.prototype = {
    _selectors: {
      contentClass: '.content',
      levelClass: '.level',
      slideClass: '.slide',

      framingClass: '.framing',
      frameId: '#frame_'
    },
    _clientPos: {
      x1: 0,
      y1: 0,
      x2: 0,
      y2: 0
    },
    _curPos: {
      'margin-left': 0,
      'top': 0
    },
    _posToReach: {
      'margin-left': 0,
      'top': 0
    },
    _curScreen: {
      model: null,
      level: 0,
      slide: 0
    },
    _structure: null,
    _curMoveDirectionId: null,
    /* _curMoveAxis
     * type:     int
     * values:   0 - Y (вертикальное движение),
     *           1 - X (горизонтальное движение)*/
    _curMoveAxis: null,

    _canMouseWheel: true,
    _needChangeSite: false,
    _needNoNormalizeHeadersOnLevel: -1,

    _animation: null,
    _content: null,
    _helpButton: null,
    _borderColors: [
      'dark-red',
      'light-blue',
      'red',
      'green',
      'golden'
    ],
    moveDirections: [
      'left',
      'up',
      'right',
      'down'
    ],
    mouseActivityStarts: 0,
    screenDragBorder: 0.3,
    isMousePressed: false,
    events: [
      'mousedown',
      'mousemove',
      'mouseup',
      'mouseleave', /*mouseup*/
      'touchstart', /*mousedown*/
      'touchmove', /*mousemove*/
      'touchend', /*mouseup*/
      'mousewheel',
      'DOMMouseScroll',
      'keydown'
    ],

    handleEvent: function (self, e) {
      if (self.events.indexOf(e.type) != -1) {
        self['_' + e.type](e);
      }
    },

    _mousedown: function (e) {
      $('body').css('cursor', 'url(img/cursors/c-drag.png), auto');

      this.setCurPos();
      this.setClientPos({
        x1: e.clientX,
        y1: e.clientY,
        x2: e.clientX,
        y2: e.clientY
      });
      this.isMousePressed = true;
      this.getContent().freePairOfHeaders();
      this.getAnimation().stop();
    },

    _mousemove: function (e) {
      if (!this.isMousePressed)
        return;
      this.setClientPos({
        x2: e.clientX,
        y2: e.clientY
      });
      if (Math.abs(Math.abs(this.getClientPosX2() - this.getClientPosX1()) - Math.abs(this.getClientPosY2() - this.getClientPosY1())) > 10) {
        this.resolveMoveDirrection();
        this.startMove();
      }
    },

    _mouseup: function (e) {
      $('body').css('cursor', 'default');
      this.finishMove();
      this.isMousePressed = false;
    },

    _mouseleave: function (e) {
      if (this.isMousePressed)
        this._mouseup(e);
    },

    _touchstart: function (e) {
      e.preventDefault();
      var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
      e.clientX = touch.pageX;
      e.clientY = touch.pageY;
      this._mousedown(e);
    },

    _touchmove: function (e) {
      e.preventDefault();
      var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
      e.clientX = touch.pageX;
      e.clientY = touch.pageY;
      this._mousemove(e);
    },

    _touchend: function (e) {
      e.preventDefault();
      this._mouseup(e);
    },

    _mousewheel: function (e) {
      if (Math.abs(e.originalEvent.wheelDelta) > 10 && this.canMouseWheel()) {

        var dX = e.originalEvent.wheelDeltaX;
        var dY = e.originalEvent.wheelDeltaY;
        var screenObj;

        if (dX) {
          if (dX > 0)
            screenObj = {slide: this.getCurSlideId() - 1};   // left
          else
            screenObj = {slide: this.getCurSlideId() + 1};   // right
        }
        else {
          if (dY > 0)
            screenObj = {level: this.getCurLevelId() - 1};   // up
          else
            screenObj = {level: this.getCurLevelId() + 1};   // down
        }

        if (this.moveToScreen(screenObj)) {
          //для мышей с тач-скроллом и тачпадов
          this.setCanMouseWheel(false);
          setTimeout(function () {
            this.setCanMouseWheel(true);
          }.bind(this), 1000);
        }
      }
    },

    _DOMMouseScroll: function (e) {
      this._mousewheel(e);
    },

    _keydown: function (e) {
      switch (e.which) {
        case 37:
          this.moveToScreen({slide: this.getCurSlideId() - 1});   // left
          break;
        case 39:
          this.moveToScreen({slide: this.getCurSlideId() + 1});   // right
          break;
        case 38:
          this.moveToScreen({level: this.getCurLevelId() - 1});   // up
          break;
        case 40:
          this.moveToScreen({level: this.getCurLevelId() + 1});   // down
          break;
        default:
          return;
      }
    },

    _moveByDelta: function (delta) {
      var isVertical = this.isVertical();
      var curVal = (isVertical ? this.getCurTop() : this.getCurLeft()) + delta;
      var cssObj = isVertical ?
        {'top': curVal} :
        {'margin-left': curVal};

      if (isVertical) {
        this.el.css(cssObj);
      }
      else {
        this.getLevel().css(cssObj);
      }

      this.getFraming().moveByValue(curVal, isVertical);
      this.getContent().moveByDelta(delta, isVertical);
    },

    _moveFuncOut: function (val) {
      return this._normalizeMoveFunc(Math.atan(5 * Math.PI * val) / 1.50722);
    },
    _moveFuncInOut: function (val) {
      return this._normalizeMoveFunc(Math.atan(5 * Math.PI * (val - 0.5)) / 2.888 + 0.5);
    },
    _normalizeMoveFunc: function (val) {
      return val < 0 ? 0 : val > 1 ? 1 : val;
    },

    resolveMoveDirrection: function () {
      var x1 = this.getClientPosX1();
      var y1 = this.getClientPosY1();
      var x2 = this.getClientPosX2();
      var y2 = this.getClientPosY2();

      this._curMoveDirectionId = null;

      if (Math.abs(x1 - x2) > Math.abs(y1 - y2)) {
        if (x1 > x2)
          this.setMoveDirection('left');
        else
          this.setMoveDirection('right');
      }
      else {
        if (y1 > y2)
          this.setMoveDirection('up');
        else
          this.setMoveDirection('down');
      }
    },

    setMoveDirection: function (moveTo) {
      if (!moveTo) {
        this._curMoveDirectionId = null;
        this._curMoveAxis = null;
      }
      else {
        var id = this.moveDirections.indexOf(moveTo);
        if (id == -1)
          return;
        if (this._curMoveAxis === null) {    //если ось движения НЕ задана
          this._curMoveDirectionId = id;
          this._curMoveAxis = id % 2;
        }
        else                                    //если задана ось движения
        if (this._curMoveAxis === id % 2    //соответствует ли новое значение текущей оси движения
          && this._curMoveDirectionId !== id) {   //не установлено ли уже такое значение
          this._curMoveDirectionId = id;
        }
      }
    },
    revertCurMoveDirection: function () {
      var val = this._curMoveDirectionId;
      this._curMoveDirectionId = val < 2 ? val + 2 : val - 2;
    },
    getCurMoveDirectionId: function () {
      return this._curMoveDirectionId;
    },

    moveByPos: function (left, top) {
      var isVertical = this.isVertical();

      var progress = Math.abs(isVertical ? top / document.body.clientHeight : left / document.body.clientWidth);
      this.getFraming().setProgress(progress);
      this.getContent().setProgress(progress);

      this._moveByDelta(isVertical ? top : left);
    },
    moveByProgress: function (progress) {
      var isVertical = this.isVertical();
      var delta = this.getDelta();
      var curPosVal = ( isVertical ? this.getCurTop() : this.getCurLeft() );
      var curSlidePos = curPosVal + delta;
      var posToReach = this.getPosToReach();
      var deltaPos = posToReach - curSlidePos;
      var tmpProgress = this.isMoveWithoutTouch() ? this._moveFuncInOut(progress) : this._moveFuncOut(progress);
      var val = delta + deltaPos * tmpProgress;

      var framing = this.getFraming();
      var content = this.getContent();
      if (curPosVal == posToReach) {
        framing.revertProgress();
        content.revertProgress();
      }
      framing.setProgress(tmpProgress, true);
      content.setProgress(tmpProgress, true);

      this._moveByDelta(val);
    },

    startMove: function () {
      this.moveByPos(this.getClientPosX2() - this.getClientPosX1(), this.getClientPosY2() - this.getClientPosY1());
    },

    finishMove: function () {
      var isVertical = this.isVertical();
      var delta = Math.abs(this.getDelta());
      var clientVal = isVertical ? document.body.clientHeight : document.body.clientWidth;
      var curProgress = delta / clientVal;
      var factor = this.getCurMoveDirectionId() < 2 ? -1 : 1;
      var newScreenVal = isVertical ? this.getCurLevelId() - factor : this.getCurSlideId() - factor;
      var needToChangeScreen = curProgress >= this.screenDragBorder;
      var canChangeScreen = newScreenVal >= 0 && newScreenVal <= (isVertical ? this.getLastModelLevel() : this.getLastModelSlide());

      if (needToChangeScreen && canChangeScreen) {  // продолжить в том же направлении
        var newCurScreenValue = isVertical ? {
          level: newScreenVal
        } : {
          slide: newScreenVal
        };
        this.changeScreen(newCurScreenValue);
      }
      else {   //откатить назад
        this.revertCurMoveDirection();
        curProgress = 1 - curProgress;
      }
      this.setPosToReach();
      var animation = this.getAnimation();
      //добавляем в animation функцию complete
      animation.setOptions({
        complete: this.finishAnimation.bind(this)
      });
      animation.start(curProgress);
    },

    finishAnimation: function () {
      var content = this.getContent();
      content.setMoveByArrows(false);
      content.freePairOfHeaders();
      content.setProgress();

      this.getFraming().setProgress();

      var unnormalizedHeadersLevel = this.getLevelToNormalizeHeders();
      if (unnormalizedHeadersLevel !== -1) {
        this.getContent().normaliseHeaders(unnormalizedHeadersLevel);
        var left = {'margin-left': 0};
        this.getLevel(unnormalizedHeadersLevel).css(left);
        this.setCurPos(left);
        this.setLevelToNormalizeHeders(-1);
      }

      if (this._needChangeSite) {
        this.setCurModelName(this.getSlide(this.getCurSlideId()).attr('data-name'));
        this.showCurModelLevels();

        pageMenu.redraw(this.getCurModel());

        this._needChangeSite = false;
      }

      this.setCursorStyle();

      this.setMoveDirection();
    },

    moveToScreen: function (screenObj) {
      if (!screenObj || typeof screenObj !== 'object')
        return;

      var animation = this.getAnimation();
      animation.stop();
      this.setClientPos();

      var levelId = screenObj.level;
      var slideId = screenObj.slide;
      var isVertical = screenObj['level'] !== undefined;

      if ((slideId >= 0 && slideId <= this.getLastModelSlide())
        || (levelId >= 0 && levelId <= this.getLastModelLevel())) {
        if (!isVertical) {
          var content = this.getContent();
          content.freePairOfHeaders();
          content.setMoveByArrows(true);
        }

        this.setCurPos();
        this.setMoveDirection(isVertical ? levelId > this.getCurLevelId() ? "up" : "down" : slideId > this.getCurSlideId() ? "left" : "right");
        this.changeScreen(screenObj);
        this.setPosToReach();

        animation.setOptions({
          complete: this.finishAnimation.bind(this)
        });
        animation.start();

        return true;
      }
      return false;
    },

    changeScreen: function (newCurScreenValue) {
      var l1 = this.getCurLevelId();
      var s1 = this.getCurSlideId();
      this.setCurScreen(newCurScreenValue);
      var wasChanged = {
        level: l1 != this.getCurLevelId() && l1 !== 0,
        slide: s1 != this.getCurSlideId()
      };
      if (wasChanged.level) {
        var newSlide = 0;
        if (this.getCurLevelId() === 0)
          newSlide = +( this.getCurModelName() == 'btl' );
        this.setCurScreen({
          slide: newSlide
        });
        this.setLevelToNormalizeHeders(l1);
      }
      if (wasChanged.slide && this.getCurLevelId() === 0) {
        this._needChangeSite = true;
      }

      pageMenu.setMenuPosition(this.getCurLevelId(), this.getCurSlideId());

      return wasChanged;
    },

    setCursorStyle: function () {
      var topBar = $('.sidebar.nav-top');
      var bottomBar = $('.sidebar.nav-bottom');
      var leftBar = $('.sidebar.nav-left');
      var rightBar = $('.sidebar.nav-right');

      this.getCurLevelId() === 0 ?
        topBar.css('cursor', 'default') : topBar.css('cursor', 'url(img/cursors/c-up.png), auto');

      this.getCurLevelId() === this.getLastModelLevel() ?
        bottomBar.css('cursor', 'default') : bottomBar.css('cursor', 'url(img/cursors/c-down.png), auto');

      this.getCurSlideId() === 0 ?
        leftBar.css('cursor', 'default') : leftBar.css('cursor', 'url(img/cursors/c-left.png), auto');

      this.getCurSlideId() === this.getLastModelSlide() ?
        rightBar.css('cursor', 'default') : rightBar.css('cursor', 'url(img/cursors/c-right.png), auto');

      //TODO перенести в контент
      var cL = this.getLevel().find('h1');

      $(cL[this.getCurSlideId()]).css('cursor', leftBar.css('cursor'));
      $(cL[this.getCurSlideId() + 1]).css('cursor', rightBar.css('cursor'));
    },

    addClicksOnHeaders: function () {
      $('h1').click(function () {
        if ($(this).parent().index() === SC.getCurSlideId()) {
          SC.moveToScreen({slide: $(this).parent().index() - 1});
        } else {
          SC.moveToScreen({slide: $(this).parent().index()});
        }
      });
    },

    resize: function () {
      var screenSize = {
        height: document.body.clientHeight,
        width: document.body.clientWidth
      };
      this.el.find(this.getSelector('slideClass')).css(screenSize);
      var levels = this.el.find(this.getSelector('levelClass'));
      for (var i = 0; i < levels.length; ++i) {
        var curLevel = levels[i];
        var $level = $(curLevel).css(
          $.extend(
            $.extend({}, screenSize), {
              width: screenSize.width * curLevel.children.length
            }
          )
        );
        var levelName = $level.attr('data-name');
        if (levelName && levelName[0] !== '_' && levelName !== this.getCurModelName())
          $level.hide();
      }
      var $curLevel = this.getLevel();
      var curPosObj = {};
      if (parseInt($curLevel.css('margin-left'))) {
        var left = {'margin-left': -this.getCurSlideId() * screenSize.width};
        $curLevel.css(left);
        $.extend(curPosObj, left);
      }
      if (parseInt(this.el.css('top'))) {
        var top = {'top': -this.getCurLevelId() * screenSize.height};
        this.el.css(top);
        $.extend(curPosObj, top);
      }
      this.setCurPos(curPosObj);

      this.getContent().resize(top);
      this.getFraming().resize(top);

      this.setPerspectiveOrigin();
    },

    setClientPos: function (obj) {
      if (!obj && typeof obj !== 'object')
        obj = {
          x1: 0,
          y1: 0,
          x2: 0,
          y2: 0
        };
      $.extend(this._clientPos, obj);
      return true;
    },
    getClientPosX1: function () {
      return this._clientPos.x1;
    },
    getClientPosY1: function () {
      return this._clientPos.y1;
    },
    getClientPosX2: function () {
      return this._clientPos.x2;
    },
    getClientPosY2: function () {
      return this._clientPos.y2;
    },

    setCurPos: function (obj) {
      if (!obj && typeof obj !== 'object')
        obj = {
          'top': parseInt(this.el.offset().top),
          'margin-left': parseInt(this.getLevel().css('margin-left') || 0)
        };
      if (obj['margin-left']) {
        if (obj['margin-left'] > 0)
          obj['margin-left'] = 0;
        var minLeft = -(this.getLastModelSlide() * document.body.clientWidth);
        if (obj['margin-left'] < minLeft)
          obj['margin-left'] = minLeft;
      }
      if (obj['top']) {
        if (obj['top'] > 0)
          obj['top'] = 0;
        var minTop = -(this.getLastModelLevel() * document.body.clientHeight);
        if (obj['top'] < minTop)
          obj['top'] = minTop;
      }
      $.extend(this._curPos, obj);
    },
    getCurLeft: function () {
      return this._curPos['margin-left'];
    },
    getCurTop: function () {
      return this._curPos['top'];
    },

    getCurMoveAxix: function () {
      return this._curMoveAxis;
    },

    setIds: function () {
      for (var name in this._structure) {
        var $levels = this.el.find(this.getSelector('levelClass') + '.' + name);
        for (var i = 0; i < $levels.length; ++i) {
          $($levels[i]).attr('index', i);
          var children = $levels[i].children;
          for (var j = 0; j < children.length; ++j) {
            $(children[j]).attr('index', j);
          }
        }
      }
    },

    getSelector: function (name) {
      return this._selectors[name] || "";
    },

    initStructure: function () {
      if (this._structure)
        return;
      this._structure = {};
      var names = this.el.find(this.getSelector('levelClass') + ':first ' + '[data-name]');
      for (var i = 0; i < names.length; ++i) {
        var name = $(names[i]).attr('data-name');
        if (!this.getCurModelName())
          this.setCurModelName(name);
        this._structure[name] = [];
      }
    },

    setStructure: function () {
      var structure = this._structure;
      for (var name in structure) {
        var $slides = this.el.find(
          this.getSelector('levelClass')
          + '.' + name + ' '
          + this.getSelector('slideClass')
        );
        var prevParentId = '';
        for (var i = 0; i < $slides.length; ++i) {
          var curParent = $($slides[i]).parent();
          var curParentId = curParent.attr('index');

          if (prevParentId === curParentId) {
            ++structure[name][structure[name].length - 1];
          }
          else {
            structure[name].push(1);
            prevParentId = curParentId;
          }
        }
      }
    },
    getModel: function (name) {
      return (this._structure && this._structure[name]) || null;
    },
    getCurModel: function () {
      return this.getModel(this.getCurModelName());
    },
    getLastModelLevel: function () {
      return this.getLevelsAmount() - 1;
    },
    getLastModelSlide: function () {
      return this.getCurModel()[this.getCurLevelId()] - 1;
    },
    getLevelsAmount: function () {
      return this.getCurModel().length;
    },

    showCurModelLevels: function ($levels) {
      $levels = $levels || this.el.find(this.getSelector('levelClass'));
      for (var i = 1; i < $levels.length; ++i) {
        var $level = $($levels[i]);
        if ($level.hasClass(this.getCurModelName()))
          $level.show();
        else
          $level.hide();
      }
    },

    setCurModelName: function (name) {
      this._curScreen.model = name;
    },
    getCurModelName: function () {
      return this._curScreen.model;
    },

    setCurScreen: function (obj) {
      if (!obj && typeof obj !== 'object')
        obj = {
          level: 0,
          slide: 0
        };
      if (obj.level) {
        if (obj.level > this.getLastModelLevel())
          obj.level = this.getLastModelLevel();
        if (obj.level < 0)
          obj.level = 0;
        obj.slide = 0;
      }
      if (obj.slide) {
        if (obj.slide > this.getLastModelSlide())
          obj.slide = this.getLastModelSlide();
        if (obj.slide < 0)
          obj.slide = 0;
      }
      $.extend(this._curScreen, obj);
    },
    getCurLevelId: function () {
      return this._curScreen.level;
    },
    getCurSlideId: function () {
      return this._curScreen.slide;
    },

    getLevel: function (id) {
      id = id || id === 0 ? id : this.getCurLevelId();
      return this.el.find(
        this.getSelector('levelClass')
        + '.' + this.getCurModelName()
        + '[index="' + id + '"]'
      );
    },
    getSlide: function (id) {
      id = id || id === 0 ? id : this.getCurSlideId();
      return this.el.find(
        this.getSelector('levelClass')
        + '.' + this.getCurModelName()
        + '[index="' + this.getCurLevelId() + '"]'
        + ' ' + this.getSelector('slideClass')
        + '[index="' + id + '"]'
      );
    },

    setPosToReach: function () {
      var factor = -1; //всегда неположительная
      this._posToReach['top'] = document.body.clientHeight * this.getCurLevelId() * factor;
      this._posToReach['margin-left'] = document.body.clientWidth * this.getCurSlideId() * factor;
    },
    getPosToReach: function () {
      return this.isVertical() ? this._posToReach['top'] : this._posToReach['margin-left'];
    },

    setCanMouseWheel: function (canMouseWheel) {
      this._canMouseWheel = canMouseWheel;
    },
    canMouseWheel: function () {
      return this._canMouseWheel;
    },

    setAnimation: function (opts) {
      this._animation = new Animation(opts);
      return this._animation;
    },
    getAnimation: function () {
      return this._animation;
    },

    setFraming: function (opts) {
      this._framing = new Framing(opts);
      return this._framing;
    },
    getFraming: function () {
      return this._framing;
    },

    setContent: function (opts) {
      this._content = new Content(opts);
      return this._content;
    },
    getContent: function () {
      return this._content;
    },

    isVertical: function () {
      return !!this.getCurMoveAxix();
    },
    getDelta: function () {
      var isVertical = this.isVertical();
      return isVertical ? this.getClientPosY2() - this.getClientPosY1() : this.getClientPosX2() - this.getClientPosX1();
    },

    setPerspectiveOrigin: function () {
      var perspectiveOrigin = 'center ' + ( document.body.clientHeight / 2 ) + 'px';
      $('body > div').css({
        '-webkit-perspective-origin': perspectiveOrigin,
        '-moz-perspective-origin': perspectiveOrigin,
        '-ms-perspective-origin': perspectiveOrigin,
        '-o-perspective-origin': perspectiveOrigin,
        'perspective-origin': perspectiveOrigin
      });
    },

    isMoveWithoutTouch: function () {
      return ( this.getClientPosX1() || this.getClientPosX2() || this.getClientPosY1() || this.getClientPosY2() ) === 0;
    },

    setLevelToNormalizeHeders: function (level) {
      this._needNoNormalizeHeadersOnLevel = level;
    },
    getLevelToNormalizeHeders: function () {
      return this._needNoNormalizeHeadersOnLevel;
    }
  };

  w.Scroll = _scroll;
})(window, document, jQuery);