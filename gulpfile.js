var gulp = require('gulp');

var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var minifyHTML = require('gulp-minify-html');

var imagemin = require('gulp-imagemin');
//var pngquant = require('imagemin-pngquant');

var del = require('del');

gulp.task('minifyhtml', function () {
  var htmlSrc = './*.html',
    htmlDst = './production';

  gulp.src(htmlSrc)
    .pipe(minifyHTML())
    .pipe(gulp.dest(htmlDst));
});

gulp.task('minifycss', function () {
  return gulp.src('./css/*.css')
    .pipe(minifycss())
    .pipe(gulp.dest('./production/css'));
});

gulp.task('minifyjs', function () {
  return gulp.src('./js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./production/js'));
});

gulp.task('minifyimg', function () {
  return gulp.src('./img-temp/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: []
    }))
    .pipe(gulp.dest('./img-compressed/'));
});

gulp.task('production', ['minifyhtml', 'minifycss', 'minifyjs']);
//gulp.task('production', ['minifyimg']);